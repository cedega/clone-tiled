CFLAGS=-O3

ifneq (,$(findstring emcc,$(CC)))

	BUILD_DIR=build-web
	CFLAGS:=$(subst -g,,$(CFLAGS))

else ifeq ($(OS),Windows_NT)

	BUILD_DIR=build

else ifeq ($(shell uname), Darwin)

	BUILD_DIR=build

else

	BUILD_DIR=build

endif

srcs := $(wildcard src/*.cpp src/*/*.cpp)
csrcs := $(wildcard src/*.c src/*/*.c)

ifneq (,$(findstring emcc,$(CC)))

objs := $(srcs:.cpp=.bc)
cobjs := $(csrcs:.c=.o)

buildobjs := $(subst src/,,$(foreach object,$(objs),$(BUILD_DIR)/$(object)))
buildcobjs := $(subst src/,,$(foreach object,$(cobjs),$(BUILD_DIR)/$(object)))
builddeps := $(buildobjs:.bc=.d) $(buildcobjs:.o=.d)

else

objs := $(srcs:.cpp=.opp)
cobjs := $(csrcs:.c=.o)
buildobjs := $(subst src/,,$(foreach object,$(objs),$(BUILD_DIR)/$(object)))
buildcobjs := $(subst src/,,$(foreach object,$(cobjs),$(BUILD_DIR)/$(object)))
builddeps := $(buildobjs:.opp=.d) $(buildcobjs:.o=.d)

endif

dirs := $(foreach sr,$(srcs),$(BUILD_DIR)/$(sr))
dirs := $(dirs) $(foreach sr,$(csrcs),$(BUILD_DIR)/$(sr))
dirs := $(sort $(foreach dr,$(subst src/,,$(dirs)),$(dir $(dr))))

all: dir compile

dir:
	mkdir -p $(dirs) lib/windows/mingw lib/mac lib/linux lib/emscripten

compile: $(buildobjs)
	@echo "Plugin has finished compiling."

$(BUILD_DIR)/%.o: src/%.c
	$(CC) $(CFLAGS) -I"../clone/include/" -I"include/" -MMD -MP -w -c $< -o $(subst src/,,$@)

$(BUILD_DIR)/%.opp: src/%.cpp
	$(CXX) $(CFLAGS) -I"../clone/include/" -I"include/" -MMD -MP -Wall -c $< -o $(subst src/,,$@)

$(BUILD_DIR)/%.bc: src/%.cpp
	$(CXX) $(CFLAGS) -I"../clone/include/" -I"include/" -MMD -MP -Wall -c $< -o $(subst src/,,$@)

clean:
	rm -rf $(BUILD_DIR)/*

-include $(builddeps)
