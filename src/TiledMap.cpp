#include <algorithm>
#include "Clone/Core/Platform.hpp"
#include "Clone/Core/Camera.hpp"
#include "Clone/Graphics/GraphicsAPI.hpp"
#include "Clone/Plugins/Tiled/TiledMap.hpp"

namespace cl
{

TiledMap::TiledMap() :
    spacing_(0),
    number_of_layers_(0),
    tiles_(NULL),
    render_layer_(0)
{

#if defined(CL_TARGET_DESKTOP)
    vertex_buffer_ = cl::GraphicsAPI::getInstance()->createGPUBuffer();
#else
    vertex_buffer_ = cl::GraphicsAPI::getInstance()->createCPUBuffer();
#endif

}

TiledMap::TiledMap(const cl::TiledMap &obj) :
    sprite_sheet_(obj.sprite_sheet_),
    level_size_(obj.level_size_),
    tile_size_(obj.tile_size_),
    spacing_(obj.spacing_),
    number_of_layers_(obj.number_of_layers_),
    render_layer_(obj.render_layer_)
{

#if defined(CL_TARGET_DESKTOP)
    vertex_buffer_ = cl::GraphicsAPI::getInstance()->createGPUBuffer();
#else
    vertex_buffer_ = cl::GraphicsAPI::getInstance()->createCPUBuffer();
#endif

    tiles_ = new int**[obj.number_of_layers_];

    for (int i = 0; i < obj.number_of_layers_; i++)
    {
        tiles_[i] = new int*[obj.level_size_.y];
        
        for (int j = 0; j < obj.level_size_.y; j++)
            tiles_[i][j] = new int[obj.level_size_.x];
    }

    for (int i = 0; i < obj.number_of_layers_; i++)
        for (int j = 0; j < obj.level_size_.y; j++)
            for (int k = 0; k < obj.level_size_.x; k++)
                tiles_[i][j][k] = obj.tiles_[i][j][k];
}

TiledMap::~TiledMap()
{
    deconstruct();
    delete vertex_buffer_;
}

TiledMap& TiledMap::operator=(const cl::TiledMap &obj)
{
    sprite_sheet_ = obj.sprite_sheet_;
    level_size_ = obj.level_size_;
    tile_size_ = obj.tile_size_;
    spacing_ = obj.spacing_;
    number_of_layers_ = obj.number_of_layers_;
    render_layer_ = obj.render_layer_;

    tiles_ = new int**[obj.number_of_layers_];

    for (int i = 0; i < obj.number_of_layers_; i++)
    {
        tiles_[i] = new int*[obj.level_size_.y];
        
        for (int j = 0; j < obj.level_size_.y; j++)
            tiles_[i][j] = new int[obj.level_size_.x];
    }

    for (int i = 0; i < obj.number_of_layers_; i++)
        for (int j = 0; j < obj.level_size_.y; j++)
            for (int k = 0; k < obj.level_size_.x; k++)
                tiles_[i][j][k] = obj.tiles_[i][j][k];

    return *this;
}

void TiledMap::deconstruct()
{   
    if (tiles_ != NULL)
    {
        for (int i = 0; i < number_of_layers_; i++)
        {
            for (int j = 0; j < level_size_.y; j++)
                delete[] tiles_[i][j];
            
            delete[] tiles_[i];
        }
        
        delete[] tiles_;
    }

    tiles_ = NULL;
}

void TiledMap::initialize(cl::SpriteSheet *sprite_sheet, cl::Shader *shader, cl::Camera *camera, const std::string &tiled_contents)
{
    sprite_sheet_ = sprite_sheet;
    cl::GenericRenderObject::setDrawComponent(cl::DrawComponent(vertex_buffer_, shader));

    cl::GenericRenderObject::setBackfaceCulling(false);
    cl::GenericRenderObject::setEnableBlending(true);

    deconstruct();
    findSizeInfo(tiled_contents);
    parseLayerInfo(tiled_contents);

    update(camera);
}

unsigned int TiledMap::bindTextures(const cl::Texture *textures[], unsigned int draw_index)
{
    textures[0] = sprite_sheet_->getTexture();
    return 1;
}

void TiledMap::update(cl::Camera *camera)
{
    // Generate data
    float window_x = glm::abs(2.0f / camera->getProjectionMatrix()[0][0]);
    float window_y = glm::abs(2.0f / camera->getProjectionMatrix()[1][1]);

    int num_tiles_x = static_cast<int>(std::ceil(window_x / sprite_sheet_->getFrameSize().x)) + 2;
    int num_tiles_y = static_cast<int>(std::ceil(window_y / sprite_sheet_->getFrameSize().y)) + 2;

    float half_x = sprite_sheet_->getFrameSize().x / 2.0f;
    float half_y = sprite_sheet_->getFrameSize().y / 2.0f;

    // Create start position for vertex data -- (0,0) will be the top-left of the TiledMap
    const float start_x = half_x;
    const float start_y = half_y;
    float pos_x = start_x;
    float pos_y = start_y;

    // Batch all the neccessary tiles
    if (num_tiles_x * num_tiles_y != static_cast<int>(batches_.size()))
    {
        batches_.resize(num_tiles_x * num_tiles_y);

        for (int i = 0; i < num_tiles_x * num_tiles_y; i++)
            batches_[i] = sprite_sheet_->getVertexBuffer();

        vertex_buffer_->createBatch(batches_);
    }

    // Determine camera position on how to display the tiles
    int move_x_tiles = static_cast<int>(camera->getInterpolatedPosition().x / sprite_sheet_->getFrameSize().x) * sprite_sheet_->getFrameSize().x; 
    int move_y_tiles = static_cast<int>(camera->getInterpolatedPosition().y / sprite_sheet_->getFrameSize().y) * sprite_sheet_->getFrameSize().y;

    if (move_x_tiles < 0)
        move_x_tiles = 0; 

    if (move_y_tiles < 0)
        move_y_tiles = 0;

    // Prevent the drawing of tiles that fall outside of the TiledMap
    glm::vec2 camera_2d(camera->getInterpolatedPosition().x, camera->getInterpolatedPosition().y);

    int start_tile_x = static_cast<int>(camera_2d.x / sprite_sheet_->getFrameSize().x);
    int start_tile_y = static_cast<int>(camera_2d.y / sprite_sheet_->getFrameSize().y);

    if (start_tile_x < 0)
        start_tile_x = 0;

    if (start_tile_y < 0)
        start_tile_y = 0;

    int clamp_x = std::min(0, level_size_.x - (start_tile_x + num_tiles_x));
    int clamp_y = std::min(0, level_size_.y - (start_tile_y + num_tiles_y));

    // Set default vertex data
    glm::vec2 default_positions[4];
    default_positions[0].x = -half_x;
    default_positions[0].y = half_y;

    default_positions[1].x = -half_x;
    default_positions[1].y = -half_y;

    default_positions[2].x = half_x;
    default_positions[2].y = -half_y;

    default_positions[3].x = half_x;
    default_positions[3].y = half_y;

    Vertex *vertices = new Vertex[4 * num_tiles_y * num_tiles_x];

    // Layout the tile map
    for (int i = 0; i < num_tiles_y; i++)
    {
        for (int j = 0; j < num_tiles_x; j++)
        {
            glm::vec2 texcoords[4];
            if (i < num_tiles_y + clamp_y && j < num_tiles_x + clamp_x)
                getTextureCoordinates(tiles_[render_layer_][start_tile_y + i][start_tile_x + j], texcoords);

            // For each vertex of a 2D image
            for (int k = 0; k < 4; k++)
            {
                vertices[4 * (i * num_tiles_x + j) + k].position = default_positions[k];
                vertices[4 * (i * num_tiles_x + j) + k].position.x += pos_x + move_x_tiles;
                vertices[4 * (i * num_tiles_x + j) + k].position.y += pos_y + move_y_tiles;
                vertices[4 * (i * num_tiles_x + j) + k].texcoord = texcoords[k];
            }

            pos_x += sprite_sheet_->getFrameSize().x;
        }

        pos_y += sprite_sheet_->getFrameSize().y;
        pos_x = start_x;
    }

    uint8_t* buffer = vertex_buffer_->map(cl::MapVertices, cl::MapWrite);
    std::memcpy(buffer, vertices, sizeof(Vertex) * 4 * num_tiles_y * num_tiles_x);
    vertex_buffer_->unmap(cl::MapVertices);

    delete[] vertices;
}

void TiledMap::clampCamera(cl::Camera *camera)
{
    float window_x = glm::abs(2.0f / camera->getProjectionMatrix()[0][0]);
    float window_y = glm::abs(2.0f / camera->getProjectionMatrix()[1][1]);

    // Clamp X-left
    if (camera->getPosition().x < 0.0f)
    {
        camera->setPosition(glm::vec3(0.0f, camera->getPosition().y, 0.0f), true);
    }
    // Clamp X-right
    else if (camera->getPosition().x > getDimensions().x - window_x)
    {
        camera->setPosition(glm::vec3(getDimensions().x - window_x, camera->getPosition().y, 0.0f), true);
    }

    // Clamp Y-up
    if (camera->getPosition().y < 0.0f)
    {
        camera->setPosition(glm::vec3(camera->getPosition().x, 0.0f, 0.0f), true);
    }
    // Clamp Y-down
    else if (camera->getPosition().y > getDimensions().y - window_y)
    {
        camera->setPosition(glm::vec3(camera->getPosition().x, getDimensions().y - window_y, 0.0f), true);
    }
}

void TiledMap::findSizeInfo(const std::string &file_contents)
{
    // Stores width of layers in level_size_.x.
    std::string size = "";
    size_t index = file_contents.find(" width=") + 8;
    
    while (file_contents.at(index) != '"')
    {
        size += file_contents.at(index);
        index++;
    }

    level_size_.x = atoi(size.c_str());
    
    // Stores height of layers in level_size_.y.
    index = file_contents.find(" height=") + 9;
    size = "";
    
    while (file_contents.at(index) != '"')
    {
        size += file_contents.at(index);
        index++;
    }

    level_size_.y = atoi(size.c_str());
    
    // Stores tilewidth in tile_size_.x.
    index = file_contents.find(" tilewidth=") + 12;
    size = "";
    
    while (file_contents.at(index) != '"')
    {
        size += file_contents.at(index);
        index++;
    }

    tile_size_.x = atoi(size.c_str());
    
    // Stores tileheight in tile_size_.y.
    index = file_contents.find(" tileheight=") + 13;
    size = "";
    
    while (file_contents.at(index) != '"')
    {
        size += file_contents.at(index);
        index++;
    }

    tile_size_.y = atoi(size.c_str());
  
    // Stores spacing in spacing_.
    index = file_contents.find(" spacing=") + 10;
    size = "";
    
    while (file_contents.at(index) != '"')
    {
        size += file_contents.at(index);
        index++;
    }

    spacing_ = atoi(size.c_str());
  
    // Finds the number of layers and saves it in number_of_layers_.
    index = 0;
    
    while (file_contents.find("<layer name=", index) != std::string::npos)
    {
        number_of_layers_++;
        index += file_contents.find("<layer name=", index) + 1;
    }
    
    tiles_ = new int**[number_of_layers_];
}

void TiledMap::parseLayerInfo(const std::string &file_contents)
{
    // Finish initializing tiles_.
    for (int i = 0; i < number_of_layers_; i++)
    {
        tiles_[i] = new int*[level_size_.y];
        
        for (int j = 0; j < level_size_.y; j++)
            tiles_[i][j] = new int[level_size_.x];
    }
    
    // Parse all layer data and put it into tiles_.
    size_t index = 0;
    for (int i = 0; i < number_of_layers_; i++)
    {
        index = file_contents.find("<data encoding=\"csv\">", index) + 21;
 
        while (file_contents.at(index) == ' ')
            index++;
        
        std::string value;
        for (int j = 0; j < level_size_.y; j++)
        {    
            for (int k = 0; k < level_size_.x; k++)
            {
                value = "";
                while (file_contents.at(index) != ',' && file_contents.at(index) != ' '  && file_contents.at(index) != '<')
                {
                    value += file_contents.at(index);
                    index++;
                }
                index++;
                tiles_[i][j][k] = atoi(value.c_str()) - 1;
            }
        }
    }
}

void TiledMap::getTextureCoordinates(int tile_id, glm::vec2 output[])
{
    int row = (tile_id / sprite_sheet_->getSpritesPerRow()) + 1;
    int col = (tile_id - (row - 1) * sprite_sheet_->getSpritesPerRow()) + 1;
    glm::vec4 rect = sprite_sheet_->getBounds(row, col);
    glm::ivec2 original_dimensions = sprite_sheet_->getTexture()->getDimensions();

    output[0].x = rect.x / original_dimensions.x;
    output[0].y = (rect.y + rect.w) / original_dimensions.y;

    output[1].x = rect.x / original_dimensions.x;
    output[1].y = rect.y / original_dimensions.y;

    output[2].x = (rect.x + rect.z) / original_dimensions.x;
    output[2].y = rect.y / original_dimensions.y;

    output[3].x = (rect.x + rect.z) / original_dimensions.x;
    output[3].y = (rect.y + rect.w) / original_dimensions.y;
}

glm::ivec2 TiledMap::getDimensions() const
{
    return tile_size_ * level_size_;
}

const glm::ivec2& TiledMap::getLevelSize() const
{
    return level_size_;
}

const glm::ivec2& TiledMap::getTileSize() const
{
    return tile_size_;
}

int TiledMap::getSpacing() const
{
    return spacing_;
}

int** TiledMap::getTiles(int layer)
{
    return tiles_[layer];
}

int TiledMap::getNumLayers() const
{
    return number_of_layers_;
}

void TiledMap::setSpriteSheet(cl::SpriteSheet *sprite_sheet)
{
    sprite_sheet_ = sprite_sheet;
}

cl::SpriteSheet* TiledMap::getSpriteSheet()
{
    return sprite_sheet_;
}

void TiledMap::setRenderLayer(int layer)
{
    render_layer_ = layer;
}

int TiledMap::getRenderLayer() const
{
    return render_layer_;
}

bool TiledMap::isOpaque() const
{
    return false;
}

bool TiledMap::useDepthTest() const
{
    return false;
}

}
