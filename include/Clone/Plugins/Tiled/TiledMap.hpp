#pragma once
#include "Clone/2D/SpriteSheet.hpp"

namespace cl
{

class Camera;

class TiledMap : public cl::GenericRenderObject
{
 public:
    TiledMap();
    TiledMap(const cl::TiledMap &obj);
    ~TiledMap();
    TiledMap& operator=(const cl::TiledMap &obj);

    void initialize(cl::SpriteSheet *sprite_sheet, cl::Shader *shader, cl::Camera *camera, const std::string &tiled_contents);
    unsigned int bindTextures(const cl::Texture *textures[], unsigned int draw_index);
    void update(cl::Camera *camera);
    void clampCamera(cl::Camera* camera);
    
    glm::ivec2 getDimensions() const;
    const glm::ivec2& getLevelSize() const;
    const glm::ivec2& getTileSize() const;
    int getSpacing() const;
    int** getTiles(int layer);
    int getNumLayers() const;

    void setSpriteSheet(cl::SpriteSheet *sprite_sheet);
    cl::SpriteSheet* getSpriteSheet();

    void setRenderLayer(int layer);
    int getRenderLayer() const;

    bool isOpaque() const;
    bool useDepthTest() const;

 private:
    cl::SpriteSheet *sprite_sheet_;
    cl::VertexBuffer *vertex_buffer_;
    
    struct Vertex
    {
        glm::vec2 position;
        glm::vec2 texcoord;
    };

    glm::ivec2 level_size_;
    glm::ivec2 tile_size_;
    int spacing_;
    int number_of_layers_;
    int ***tiles_;
    int render_layer_;

    std::vector<cl::VertexBuffer*> batches_;

    void deconstruct();
    void getTextureCoordinates(int tile_id, glm::vec2 output[]);
    void findSizeInfo(const std::string &file_contents);
    void parseLayerInfo(const std::string &file_contents);
};

}
